//
//  Teste.swift
//  Teste_CI_CD
//
//  Created by Marcelo I Bello on 08/08/17.
//  Copyright © 2017 Marcelo I Bello. All rights reserved.
//

import Foundation

class Teste {
    
    func multiply (_ a: Int, b: Int) -> Int {
        var ans = 0
        
        for _ in 0 ..< a {
            for _ in 0 ..< b {
                ans += 1
            }
        }
        
        return ans
    }
    
    

}
