//
//  ViewController.swift
//  Teste_CI_CD
//
//  Created by Marcelo I Bello on 06/08/17.
//  Copyright © 2017 Marcelo I Bello. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var labelStatus: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        #if Debug
            self.labelStatus.text = "Debug mode"
        #elseif Staging
            self.labelStatus.text = "Staging mode"
        #elseif Release
            self.labelStatus.text = "Release mode"
        #endif
        
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

